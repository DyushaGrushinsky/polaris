extends Node

var thrustersList:= [
		{
			"id": "E3",
			"mass": 5,
			"powerDraw": 2480
		},
		{
			"id": "D3",
			"mass": 2,
			"powerDraw": 2790
		},
		{
			"id": "C3",
			"mass": 5,
			"powerDraw": 3100
		},
		{
			"id": "B3",
			"mass": 8,
			"powerDraw": 3410
		},
		{
			"id": "A3",
			"mass": 5,
			"powerDraw": 3720
		},
		{
			"id": "A4",
			"mass": 10,
			"powerDraw": 4920,
			"minM": 210,
			"optM": 420,
			"maxM": 630
		},
		{
			"id": "E7",
			"mass": 80,
			"powerDraw": 6080
		},
		{
			"id": "D7",
			"mass": 32,
			"powerDraw": 6840
		},
		{
			"id": "C7",
			"mass": 80,
			"powerDraw": 7600
		},
		{
			"id": "B7",
			"mass": 128,
			"powerDraw": 8360
		},
		{
			"id": "A7",
			"mass": 80,
			"powerDraw": 9120
		},
		{
			"id": "A8",
			"mass": 160,
			"powerDraw": 10800
		}
	]
func find(arr: Array, target: Dictionary):
	for value in arr:
		for key in target:
			if value[key] == target[key]:
				return value
				
func get_thruster(var classN):
	return find(thrustersList, {"id": classN})
