extends RigidBody2D

var inputVector2 = Vector2.ZERO


func _physics_process(delta):
	inputVector2 = get_local_mouse_position()
	angular_velocity += inputVector2.angle()
	if Input.is_action_pressed("ui_attack"):
		$line.set_visible(true)
	else:
		$line.set_visible(false)
