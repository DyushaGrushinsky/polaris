extends RigidBody2D

var inputVector = Vector2.ZERO
var inputVector2 = Vector2.ZERO

var baseForce
var acceleraionMax
var acceleration
var phiMax1 = 5

enum TYPETHRUST { E3,D3,C3,B3,A3,A4,E7,D7,C7,B7,A7,A8 }
export(TYPETHRUST) var thrusterType = TYPETHRUST.A4

var thruster
var max_speed = 0
func _ready():
	$CollisionPolygon2D.polygon = $Path2D.curve.tessellate()
	thruster = Modules.get_thruster(TYPETHRUST.keys()[thrusterType])
	mass += thruster.mass
	max_speed = Entity.logWithBase(75600/thruster.optM, 10) * 50 * thruster.maxM / mass
		
	baseForce = thruster.powerDraw * 2
	acceleraionMax = baseForce/mass
	print(TYPETHRUST.keys()[thrusterType])
	
func move_loop():
	var direction = inputVector
	var global_direction = global_transform.basis_xform(inputVector)
	if inputVector != Vector2.ZERO:
		if get_linear_velocity().length() < max_speed:
			set_applied_force(baseForce*global_direction)
		else:
			set_applied_force(-get_linear_velocity()*200)
	else:
		if get_linear_velocity().length() > 1:
			set_applied_force(baseForce*-get_linear_velocity().normalized())
		else:
			set_applied_force(baseForce*-get_linear_velocity())
	print(get_linear_velocity().length())
		
	
func angular_loop():
	acceleration = acceleraionMax*(rad2deg(inputVector2.angle())/phiMax1)
	if acceleration < 0:
		acceleration = max(acceleration, -acceleraionMax)
	if acceleration > 0:
		acceleration = min(acceleration, acceleraionMax)
	angular_velocity += deg2rad(acceleration/2)
	
func controls_loop():
	#\\---------------------------WASD---------------------------\\
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	
	inputVector.y = -int(LEFT) + int(RIGHT)
	inputVector.x = int(UP)*2 - int(DOWN)
	#inputVector.y /= 2
	
	#inputDash = Input.is_action_just_pressed("ui_dash")
	
	
	#\\--------------------------Gamepad-------------------------\\
	#inputVector = Vector2(Input.get_joy_axis(0,JOY_AXIS_0), Input.get_joy_axis(0, JOY_AXIS_1))
	#inputVector2 = Vector2(Input.get_joy_axis(0,JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3))
	
	#\\---------------------------Mouse--------------------------\\
	inputVector2 = get_local_mouse_position()

