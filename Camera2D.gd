extends Camera2D

var zoomfactor = 1.0
var zoomspeed = 10.0


func _ready():
	pass
	
	
func _process(delta):
	zoom.x = lerp(zoom.x, zoom.x * zoomfactor, zoomspeed * delta)
	zoom.y = lerp(zoom.y, zoom.y * zoomfactor, zoomspeed * delta)


func _input(event):
	if event is InputEventMouseButton:
		if event.is_pressed():
			if event.button_index == BUTTON_WHEEL_UP:
				zoomfactor -= 0.01
			if event.button_index == BUTTON_WHEEL_DOWN:
				zoomfactor += 0.01
